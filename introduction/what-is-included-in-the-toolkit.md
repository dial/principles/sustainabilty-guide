# What is included in the Toolkit?

The **Business Model Sustainability (BMS) Toolkit** consists of a business model diagnostic tool, a Business Model Sustainability Canvas, and a Business Model Sustainability Guide. 

## Business Model Sustainability (BMS) Canvas

The BMS Canvas is a visual framework for how an organisation works. It illustrates what the organisation does, for and with whom, the resources it needs to do that and how funds flow in and out of the organisation. The BMS canvas is made up of nine building blocks of the Business Model Canvas, which were identified by Alex Osterwalder and Yves Pigneur, and two additional building blocks on “Organisational Development” and “End Game” that are from our experience fundamental to developing a sustainable business model.  

The eleven key building blocks include value proposition/solution, customer segments, channels/sales, customer relations, revenue streams, key activities, key resources, partners, cost structure, organisational development, and end game.  

## Business Model Sustainability (BMS) Guide

The BMS Guide aims to provide organisations developing digital solutions with practical and actionable guidance on each of the building blocks. Each building block section in the guide includes most of the following: 

- an overview of the building block and instructional guidance
- illustrative examples (or case studies) from real organisations 
- interactive tools that capture inputted data
  - Note: external tools are also linked, but data will not be automatically captured. 
- additional key resources for more information, and 
- links to relevant Principles for Digital Development. 
