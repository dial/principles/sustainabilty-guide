# Who is the Toolkit for?

- **Digital Product/Service Owners/Managers**: The intra- and entrepreneurs who have driven the development of a digital product and service, and are grappling with how to build a sustainable business model.
- **Advisors to these Digital Product Owners/Managers**, such as mentors, consultants, accelerator and lab managers who support digital product/service owners and managers.

<!-- TKTK: Flowchart graphic -->